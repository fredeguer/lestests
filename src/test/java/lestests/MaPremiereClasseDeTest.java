package lestests;

import org.junit.Assert;
import org.junit.Test;

public class MaPremiereClasseDeTest {
	
	@Test
	public void abs_QuandNombrePositif_AlorsLeResultatEstLeNombre() {
		double leNombre = 111;
		
		double resultat = Math.abs(leNombre);
		
		Assert.assertEquals(leNombre, resultat, 0);
	}
	
	@Test
	public void abs_QuandNombreNegatif_AlorsLeResultatEstLaValeurAbsolue() {
		double leNombre = -1234;
		
		double resultat = Math.abs(leNombre);
		
		Assert.assertEquals(1234, resultat, 0);
	}

	@Test
	public void abs_QuandNombreZero_AlorsLeResultatEstZero() {
		double leNombre = 0;
		
		double resultat = Math.abs(leNombre);
		
		Assert.assertEquals(0, resultat, 0);
	}
	
	
	// UN SUPER COMMENTAIRE MOUAHHAAHAHAHA!
	// EDDY EST PASSER PAR LAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
}
