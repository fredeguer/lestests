package exo;

public class Fruit
{
	private String name;

	public Fruit(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Fruit [name=");
		builder.append(name);
		builder.append("]");
		return builder.toString();
	}
	
	
}
